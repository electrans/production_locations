# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Location']


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    production_input_location = fields.Many2One('stock.location', 'Production Intput',
                                                states={
                                                    'invisible': Eval('type') != 'production',
                                                    'readonly': ~Eval('active'),
                                                },
                                                domain=[
                                                    ('type', 'in', ['storage', 'view'])
                                                ],
                                                depends=['type', 'active', 'id'])

    production_output_location = fields.Many2One('stock.location', 'Production Output',
                                                 states={
                                                     'invisible': Eval('type') != 'production',
                                                     'readonly': ~Eval('active'),
                                                 },
                                                 domain=[
                                                     ('type', 'in', ['storage', 'view'])
                                                 ],
                                                 depends=['type', 'active', 'id'])
