# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import production
from . import location


def register():
    Pool.register(
        location.Location,
        production.Production,
        module='electrans_production_locations', type_='model')
